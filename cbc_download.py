import bs4 as bs
import requests
import urllib.request


def get_links(tags):
    links = []
    for tag in tags:
        if '.mp3' in tag['href'] and '(short)' not in tag.get_text():
            links.append(tag['href'])
    return links
    
def download_files(links):
    print(len(links), 'files in total.')
    count = 0
    for link in links:
        count += 1
        name = str(count)
        while len(name) < 2:
            name = '0' + name
        full_name = 'cbc_' + name + '.mp3'
        print('Downloading', link)
        print('Saving as', full_name)
        urllib.request.urlretrieve(link, full_name)
        print()
    print('Downloaded', len(links), 'files successfully.')
        
def main():
    url = requests.get('http://www.cbc.ca/radio/podcasts/current-affairs-information/cross-country-checkup/')
    page = bs.BeautifulSoup(url.content, 'html.parser')
    tags = page.find_all('a', href=True)
    links = get_links(tags)
    print(links)
    download_files(links)
    
main()
