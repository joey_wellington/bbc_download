import bs4 as bs
import requests
import os.path
import urllib.request


def get_links(base_url, page_num):
    links = []
    for num in range(1, page_num + 1):
        full_url = base_url + str(num)
        url = requests.get(full_url)
        page = bs.BeautifulSoup(url.content, 'html.parser')
        # Divide the page by chunks with mp3 links and description.
        tags = page.find_all('div', attrs={'class': 'programme__body'})
        for tag in tags:
            # 'h4' is the description tag.
            if 'Karl' not in tag.find('h4').text:
                # 'a' is the links part.
                for a in tag.find_all('a'):
                    file_link = a.get('href')
                    if 'download/proto' in file_link:
                        links.append(file_link)
                        print(file_link)
    return links


def download_files(links):
    print(len(links), 'files in total.')
    print()
    count = 0
    for link in links:
        count += 1
        # Add zeros to the beginning of the file.
        name = str(count)
        while len(name) < 2:
            name = '0' + name
        full_name = 'bbc_daily_' + name + '.mp3'
        # If file exists, move on to the next.
        if os.path.isfile(full_name):
            print(full_name, 'file exists. Passing to the next.')
        else:
            print('Downloading', link)
            print('Saved as', full_name)
            urllib.request.urlretrieve(link, full_name)
        print()
    print('Downloaded', len(links), 'files successfully.')


def main():
    # Base url and number of all the pages with links.
    base_url = 'http://www.bbc.co.uk/programmes/p02nrsmt/episodes/downloads?page='
    page_num = 2
    links = get_links(base_url, page_num)
    download_files(links)
    
main()

